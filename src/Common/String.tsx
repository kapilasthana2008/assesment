export const string = {
    Login: {
        Login: 'Login',
        EmailPlaceholder:'Email',
        PasswordPlaceholder:'Password',
        SignUp: 'If you dont have an account? SignUp',
        Register:'Register',
        ConfirmPassword: 'Confirm Password',
        FirstName:'First Name',
        LastName: 'Last Name',
        SignOut:'Logout'
    }
}

export const Screens = {
    LoginScreen: 'Login',
    Registraion: 'Register',
    Home: 'Home',
    Postdetails: 'PostDetails'
}

export const Errors = {
    nonEmptyField: 'please fill all fields'
}
export const Post = {
    PostDetails:'Post Details',
    ItemTitle:'Post Title',
    ItemDescription:'Description',
    
}
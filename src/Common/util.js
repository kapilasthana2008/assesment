
import { colors } from './colors';
import snackbar from 'react-native-snackbar'
export const showSnack = text => snackbar.show({
  text:text,
  backgroundColor:colors.ORANGE_COLOR,
  duration:3000,
})
import React, { useEffect, useState } from "react";
import { View, Text, TouchableOpacity, SafeAreaView, FlatList, ActivityIndicator } from "react-native";
import { string } from "../../Common/String";
import { signOut } from '../../Services/Auth'
import { useStyle } from "./style";
import Post from "./Posts";
import { colors } from "../../Common/colors";
const Home = (props) => {
    const style = useStyle()
    const [data, setData] = useState([])
    const [activity, setActivity] = useState(true)
    useEffect(() => {

        fetch('https://jsonplaceholder.typicode.com/posts')
            .then(response => response.json())
            .then(async data => {
                if (data) {
                    await setData(data)

                }
            })
    }, [])


    return (
        <SafeAreaView style={style.safeAreaView}>
            <View style={style.header}>
                <View>
                    <Text style={style.posthedding}>Posts</Text>
                </View>
                <TouchableOpacity
                    onPress={() => signOut()}
                    style={style.logoutBtn}>
                    <Text style={style.logoutText}>{string.Login.SignOut}</Text>
                </TouchableOpacity>
            </View>
            {
                data.length === 0 ?
                    <View style={style.activityStyle}>
                        <ActivityIndicator size="large" color={colors.ORANGE_COLOR} />
                    </View>
                    :
                    <View style={style.mainContainer}>
                        {console.log("getting data", data)}
                        <FlatList
                            data={data}
                           
                            renderItem={({ item, index }) => (
                                <Post item={item} index={index} props={props}/>
                            )}
                        />
                    </View>
            }

        </SafeAreaView>
    )
}

export default Home
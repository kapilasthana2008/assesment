import React from "react";
import { View, TouchableOpacity, Text } from 'react-native'
import { useStyle } from "./style";

const Post = (props) => {
const style = useStyle(props.index)
const {navigation} = props.props
return (
        <TouchableOpacity
        onPress={()=>navigation.navigate("PostDetails",{item:props.item})}
        style={style.postView}>
            <Text>
                {props.item.title}
            </Text>
        </TouchableOpacity>
    )
}
export default Post
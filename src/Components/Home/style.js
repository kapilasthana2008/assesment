import React from 'react';
import { StyleSheet } from 'react-native';
import {colors} from '../../Common/colors';

export const useStyle = (index) => {
    console.log("getting index",index);
 return StyleSheet.create({
 
    safeAreaView:{
        flex:1,
    },
    header:{
        padding:15,
        flexDirection:"row",
        justifyContent:'space-between',
        borderBottomWidth:1,
      borderBottomColor:colors.ORANGE_COLOR,
      backgroundColor:colors.WHITE_COLOR
    },
    posthedding:{
        fontSize:18,
        fontWeight:'700'
    },
    logoutBtn:{
        backgroundColor:colors.ORANGE_COLOR,
        padding:5,
        width:'30%',
        justifyContent:'center',
        alignItems:'center',
        borderRadius:10
    },
    logoutText:{
        color:colors.WHITE_COLOR,
        fontWeight:'500',
        fontSize:15
    },
    mainContainer:{
        padding:10,
        marginBottom:20
    },
    postView:{
        borderBottomColor:colors.ORANGE_COLOR,
        borderBottomWidth:1,
        padding:10,
        backgroundColor:index%2 == 0 ? colors.ORANGE_COLOR:"",
        borderRadius:10
    },
    activityStyle:{
        justifyContent:'center',
       flex:1
    }
 })
};


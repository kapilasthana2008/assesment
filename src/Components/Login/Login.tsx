import React, { useState } from 'react';
import {
    Text,
    View,
    Button,
    TextInput
} from 'react-native';
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';
import { SafeAreaView } from 'react-native-safe-area-context';
import { string } from "../../Common/String";
import { useStyle } from "./style";

import { signUp, signIn } from '../../Services/Auth'
const Login = (props) => {
    const style = useStyle()
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')

    const onLogin = () => {
        signIn(email, password)
    }

    return (
        <SafeAreaView style={style.safeAreaView}>

            <View style={style.inputView}>
                <TextInput
                    placeholder={string.Login.EmailPlaceholder}
                    style={style.textInput}
                    onChangeText={(text) => setEmail(text)}
                />
                <TextInput
                    placeholder={string.Login.PasswordPlaceholder}
                    secureTextEntry={true}
                    style={style.textInput2}
                    onChangeText={(text) => setPassword(text)}

                />
                <TouchableOpacity style={style.btnStyle} onPress={() => onLogin()}>
                    <Text style={style.btnText}>{string.Login.Login}</Text>
                </TouchableOpacity>
                <TouchableOpacity style={style.btnStyle} onPress={() =>props.navigation.navigate('Register')}>
                    <Text style={style.btnText}>{string.Login.SignUp}</Text>
                </TouchableOpacity>
            </View>

        </SafeAreaView>

    )
}

export default Login
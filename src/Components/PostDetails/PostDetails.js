import React from 'react'
import { View, Text, SafeAreaView, TouchableOpacity, ScrollView } from 'react-native'
import { useStyle } from './style'
import { string, Post } from '../../Common/String'

const PostDetails = (props) => {
    const style = useStyle()
    const { item } = props.route.params ? props.route.params : ""
    return (
        <SafeAreaView style={style.safeAreaView}>
            <View style={style.header}>
                <View>
                    <Text style={style.posthedding}>{Post.PostDetails}</Text>
                </View>
                <TouchableOpacity
                    style={style.logoutBtn}
                    onPress={() => signOut()}
                >
                    <Text>{string.Login.SignOut}</Text>
                </TouchableOpacity>
            </View>
            <ScrollView>
                <View style={style.mainContainer}>
                    <View>
                        <Text style={style.HeadingText}>{Post.ItemTitle}</Text>
                        <View style={style.titleView}>
                            <Text>{item.title}</Text>
                        </View>

                    </View>
                    <View style={style.descriptionPost}>
                        <Text style={style.HeadingText}>{Post.ItemDescription}</Text>
                        <View style={style.titleView}>
                            <Text style={style.descriptionText}>{item.body}</Text>
                        </View>

                    </View>

                </View>

            </ScrollView>
        </SafeAreaView>
    )
}

export default PostDetails
import React from 'react';
import { StyleSheet } from 'react-native';
import {colors} from '../../Common/colors';

export const useStyle = () => {
  
 return StyleSheet.create({
 
    safeAreaView:{
        flex:1,
      
    },
    header:{
        padding:15,
        flexDirection:"row",
        justifyContent:'space-between',
        borderBottomWidth:1,
      borderBottomColor:colors.ORANGE_COLOR,
      backgroundColor:colors.WHITE_COLOR
    },
    posthedding:{
        fontSize:18,
        fontWeight:'700'
    },
    logoutBtn:{
        backgroundColor:colors.ORANGE_COLOR,
        padding:5,
        width:'30%',
        justifyContent:'center',
        alignItems:'center',
        borderRadius:10
    },
    logoutText:{
        color:colors.WHITE_COLOR,
        fontWeight:'500',
        fontSize:15
    },
    mainContainer:{
        padding:10,
        marginBottom:20
    },
    HeadingText:{
        fontSize:22,
        fontWeight:'700',
        letterSpacing:1,
    },
    titleView:{
        padding:10,
        borderRadius:10,
        borderColor:colors.ORANGE_COLOR,
        borderWidth:1,
        margintop:10,
        backgroundColor:colors.ORANGE_COLOR
    },
    descriptionPost:{
        marginTop:10
    },
    descriptionText:{
    textAlign:'justify',
   
    }
 })
};


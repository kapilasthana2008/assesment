import React, { useState } from 'react';
import {
    Text,
    View,
    Button,
    TextInput
} from 'react-native';
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';
import { SafeAreaView } from 'react-native-safe-area-context';
import { string } from "../../Common/String";
import { useStyle } from "./style";
import { signUp } from '../../Services/Auth'

const Registration = (props) => {
    const style = useStyle()
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [name , setName] = useState('')
    const [LastName , setLastName] = useState('')

    const OnRegisterClick = () => {
        const fullName = name +" "+LastName
        signUp(fullName, email, password)
    }

    return (
        <SafeAreaView style={style.safeAreaView}>

            <View style={style.inputView}>
                <TextInput
                    placeholder={string.Login.FirstName}
                    style={style.textInput}
                    onChangeText={(text) => setName(text)}
                />
                 <TextInput
                    placeholder={string.Login.LastName}
                    style={style.textInput}
                    onChangeText={(text) => setLastName(text)}
                />
                 <TextInput
                    placeholder={string.Login.EmailPlaceholder}
                    style={style.textInput}
                    onChangeText={(text) => setEmail(text)}
                />
                <TextInput
                    placeholder={string.Login.PasswordPlaceholder}
                    secureTextEntry={true}
                    style={style.textInput2}
                    onChangeText={(text) => setPassword(text)}
                />
                 <TextInput
                    placeholder={string.Login.ConfirmPassword}
                    secureTextEntry={true}
                    style={style.textInput2}
                    // onChangeText={(text) => setPassword(text)}
                />
                <TouchableOpacity style={style.btnStyle} onPress={() => OnRegisterClick()}>
                    <Text style={style.btnText}>{string.Login.Register}</Text>
                </TouchableOpacity>
            </View>

        </SafeAreaView>

    )
}

export default Registration
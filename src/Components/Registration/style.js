import React from 'react';
import { StyleSheet } from 'react-native';
import {colors} from '../../Common/colors';
export const useStyle = () => {
 return StyleSheet.create({
    safeAreaView:{
        flex: 1,
        justifyContent:'center',
        alignItems:'center'
    },
    textInput:{
        borderBottomWidth:1,
        borderBottomColor:colors.ORANGE_COLOR,
        width:'100%'
    },
    inputView:{
        width:'85%',
        
    },
    textInput2:{
        borderBottomWidth:1,
        borderBottomColor:colors.ORANGE_COLOR,
        width:'100%',
        marginTop:15
    },
    btnStyle:{
        backgroundColor:colors.ORANGE_COLOR,
        padding:10,
       alignItems:'center',
       marginTop:20,
       borderRadius:10
    },
    btnText:{
        fontWeight:'500',
        fontSize:15,
        color:colors.WHITE_COLOR
    }
 })
};


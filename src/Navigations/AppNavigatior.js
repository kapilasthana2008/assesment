
import React, { useState, useEffect } from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';
import Home from '../Components/Home/Home';
import { Screens } from '../Common/String';
import PostDetails from '../Components/PostDetails/PostDetails';

const Stack = createStackNavigator()

export default AppNavigator = () => {
    return (
        <Stack.Navigator screenOptions={{
            headerShown: false
        }}>
            <Stack.Screen
                name={Screens.Home}
                component={Home}
            />
            <Stack.Screen
                name={Screens.Postdetails}
                component={PostDetails}
            />
        </Stack.Navigator>
    )
}
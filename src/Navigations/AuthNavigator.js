
import React,{useState,useEffect} from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';
import Login from '../Components/Login/Login';
import { Screens } from '../Common/String';
import Registration from '../Components/Registration/Registration';

const Stack = createStackNavigator()

export default AppNavigator = () =>{
   return (
    <Stack.Navigator 
    screenOptions={{
        headerShown:false
    }}
    >
    <Stack.Screen 
    name={Screens.LoginScreen}
    component={Login}
    />
    <Stack.Screen 
    name='Register'
    component={Registration}
    />
</Stack.Navigator>
   )
}
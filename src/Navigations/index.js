import React,{useState,useEffect} from "react";
import { NavigationContainer } from "@react-navigation/native";
import AppNavigatior from "./AppNavigatior";
import AuthNavigator from "./AuthNavigator";
import auth from '@react-native-firebase/auth'

export default AppContainer = () =>{
    const [initializing , setInitializing] = useState(true)
    const [user , setUser] = useState()
    
    function onAuthStateChanged(user){
        setUser(user)
        if(initializing) setInitializing(false)
    }

    useEffect(()=>{
        const subscribe =  auth().onAuthStateChanged(onAuthStateChanged)
        return subscribe
    },[])
    
    if(initializing) return null
    return (
<NavigationContainer>
    {user? <AppNavigatior/>:<AuthNavigator/>}
    {/* <AuthNavigator/> */}
</NavigationContainer>
    )
}

import auth from '@react-native-firebase/auth'
import {showSnack} from '../Common/util'
import { Errors } from '../Common/String';

export const signUp = (name,email,password)=>{

    if(!name || !email || !password){
        showSnack(Errors.nonEmptyField)
        return
    }
    return auth().createUserWithEmailAndPassword(email,password).then((cred)=>{
        const {uid} = cred.user
        auth().currentUser.updateProfile({
            displayName: name
        })
        return uid
    }).catch((e)=>{
        showSnack(e.message)
    })
}

export const signIn = (email,password) =>{
    console.log("getting email ",email,'\n',password);
    if(!email || !password){
        showSnack(Errors.nonEmptyField)
    }else{
        return auth().signInWithEmailAndPassword(email,password).then((data)=>{
            console.log("getting data ",data);
            if(data !== ""){
             return data.user._user.uid
            }
         }).catch((error)=>{
            showSnack(error.message)
         })
    }
   
}

export const signOut =( ) =>{
    return auth().signOut()
}